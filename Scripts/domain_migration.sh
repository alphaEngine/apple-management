#!/bin/sh

# Written to move users from one AD domain to a new one 



LOGPATH='/Library/Logs'   
if [[ ! -d "$LOGPATH" ]]; then
    mkdir $LOGPATH
fi
set -xv; exec 1> $LOGPATH/jssMoveDomains.log 2>&1  
version=1.1
oldAD='<domain_name>'
currentAD=`dsconfigad -show | grep -i "active directory domain" | awk '{ print $5 }'`

# Let the user know what we are doing

banner=`/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType hud -title "Domain Migration" -heading "<Company Domain Migration>" -description "We are moving your user account to the new authentication domain.  When it is completed, and your computer restarts, you will be able to login to your computer with your new domain credentials."  -button1 "Proceed" -button2 "Not Now" -defaultButton 1 -cancelButton 2 -timeout 60 -countdown` 

if [[ $banner == "2" ]]; then

    echo "User canceled the move."
    exit 1

fi

# Grab current user name
loggedInUser=`/bin/ls -l /dev/console | /usr/bin/awk '{ print $3 }'`



# Unbind from AD
# Check to see if we are bound to our current AD or not.  If not we can skip this

if [[ "$currentAD" = "$oldAD" ]]; then

    

    dsconfigad -force -remove $oldAD -u <user_account> -p <user_password   

fi

# remove the current user from the machine so we get the proper UID assigned in dscl
dscl . delete /Users/$loggedInUser

# Bind to new AD Using JAMF Policy

jamf policy -id <policy_id_goes_here>


# Get the Active Directory Node Name
adNodeName=`dscl /Search read /Groups/Domain\ Users | awk '/^AppleMetaNodeLocation:/,/^AppleMetaRecordName:/' | head -2 | tail -1 | cut -c 2-`


# Get the Domain Users groups Numeric ID

domainUsersPrimaryGroupID=`dscl /Search read /Groups/Domain\ Users | grep PrimaryGroupID | awk '{ print $2}'`

accountUniqueID=`dscl "$adNodeName" -read /Users/$loggedInUser 2>/dev/null | grep UniqueID | awk '{ print $2}'`

# Reset Permissions
chown -R $accountUniqueID:$domainUsersPrimaryGroupID /Users/$loggedInUser

##  Using createmobileaccount
    cp -R /System/Library/User\ Template/English.lproj /Users/$loggedInUser
    chown -R $loggedInUser:staff /Users/$loggedInUser
    /System/Library/CoreServices/ManagedClient.app/Contents/Resources/createmobileaccount -n $loggedInUser

### Restart the computer

shutdown -r now
