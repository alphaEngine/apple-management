#!/bin/bash
exec 2>&1

#First Run script
#Written By Daniel Cintron 10/2/2013

#Recognize new network connections in system preferences
networksetup -detectnewhardware
sleep 25

setName=`networksetup -getcomputername`
scutil --set ComputerName ${setName}
scutil --set LocalHostName ${setName}
scutil --set HostName ${setName}

#Enabling Apple Remote Desktop access
/System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -activate -configure -allowAccessFor -specifiedUsers -restart -agent
/System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -configure -access -on -privs -all -users corpadmin
sleep 5

#Set Variables For JSS Enrollment
EnrollLD="/Library/LaunchDaemons/com.jamfsoftware.firstrun.enroll.plist"
EnrolWait=$(( 8 * 60 ))
EnrolWaitIncrement=30

# Make sure the computer has enrolled

#Enrolling computer in JSS

echo "Checking to see if JAMF enroll.sh is still running"

while [ -e "$EnrollLD" ]; do
    if [ $EnrolWait -le 0 ]; then
        echo "Reached wait timeout of ${EnrolWait} seconds!"
        break
    fi
    
    echo "Still not complete. Waiting another ${EnrolWaitIncrement} seconds..."
    sleep $EnrolWaitIncrement
    (( EnrolWait -= $EnrolWaitIncrement ))
done

#Add Machine Info To Login Window
defaults write /Library/Preferences/com.apple.loginwindow AdminHostInfo Time
defaults write /Library/Preferences/com.apple.loginwindow AdminHostInfo IPAddress
defaults write /Library/Preferences/com.apple.loginwindow AdminHostInfo HostName

#Stop Printer Sharing
lpadmin -p ALL -o printer-is-shared="False"

# Set default screensaver settings
/bin/mkdir -p /System/Library/User\ Template/English.lproj/Library/Preferences/ByHost

# Enable a default Screen Saver
defaults write ~/Library/Preferences/ByHost/com.apple.screensaver.*.plist idleTime 600
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/ByHost/com.apple.screensaver.$my_UUID "moduleName" -string "Arabesque.saver" 
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/ByHost/com.apple.screensaver.$my_UUID "modulePath" -string "/System/Library/Screen Savers/Arabesque.saver" 
defaults -currentHost write com.apple.screensaver askForPassword -int 1
defaults -currentHost write com.apple.screensaver idleTime -int 600
defaults -currentHost write com.apple.screensaver askForPasswordDelay -int 0

#Disable time machine pop up for external drives
defaults write /Library/Preferences/com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool true
defaults write /Library/Preferences/com.apple.TimeMachine AutoBackup 0

#Disable AirDrop By Default
defaults write /System/Library/User\ Template/English.lproj/Library/Preferences/com.apple.NetworkBrowser.plist DisableAirDrop -bool YES
sudo ifconfig awdl0 down

#Bypass MDM log in check for 10.9.2
defaults write /Library/Preferences/com.apple.mdmclient BypassPreLoginCheck -bool YES

#Set Network Time Server and Update Location
systemsetup -settimezone America/New_York

systemsetup -setnetworktimeserver us.pool.ntp.org

systemsetup -setusingnetworktime on

#Disables Bluetooth.
defaults write /Library/Preferences/com.apple.MCXBluetooth DisableBluetooth -dict state -string 'always' -bool YES
defaults write /Library/Preferences/com.apple.Bluetooth.plist ControllerPowerState -int 0

#Adjust AD log in authentication from 30 to 15 Seconds
sudo defaults write /Library/Preferences/com.apple.loginwindow DSBindTimeout -int 15

#Make Safari Open To Homepage Not Top Sites
defaults write ~/Library/Preferences/com.apple.Safari AlwaysRestoreSessionAtLaunch -bool false
defaults write ~/Library/Preferences/com.apple.Safari NewWindowBehavior -integer 0

#Set Homepage in Safari and Firefox
jamf setHomePage -homepage https://blog.360i.com -feu
sleep 5

#Hide Admin Account
sudo chown -R <admin_account> /Users/<admin_account>/
sudo defaults write /Library/Preferences/com.apple.loginwindow Hide500Users -bool YES

#Install Mcafee Agent
/Library/Application\ Support/McAfee/install.sh -i

#iWork Registration Removal
defaults write /Library/Preferences/com.apple.iWork09 RegistrationHasBeenSent -bool YES
echo "iWork Settings complete."