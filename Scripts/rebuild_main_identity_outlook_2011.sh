#! /bin/bash

# $3 is the logged in user - default for most policies.
# Created by Daniel Cintron 3/31/15 for helpdesk

currentUser=$(/bin/ls -l /dev/console | /usr/bin/awk '{ print $3 }')

/usr/bin/osascript <<ENDofOSAscript

tell application "Finder"
    activate
display dialog "All Open Office Applications will now quit for Microsoft Outlook Database Rebuilding"
end tell

set apps to {"Microsoft Excel", "Microsoft PowerPoint", "Microsoft Word","Microsoft Outlook", "Microsoft Database Daemon"}
repeat with thisApp in apps
	tell application thisApp to quit
end repeat

tell application "Microsoft Database Utility"
	activate
	
	rebuild "Main Identity"
	
end tell

ENDofOSAscript

Exit 0