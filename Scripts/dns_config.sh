#!/bin/bash



#Written By Daniel Cintron for <Company>
#May 3,2018 Incident: ITSUPPORT-18187


banner=`/Library/Application\ Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper -windowType hud -title "DNS Settings Configuration" -heading "Oscar Health Insurance Required DNS Settings" -description "We are making a required DNS change. This change will improve your devices security. Unfortunately your Pritunl will quit it's current session while the changes are completed. The application will relaunch when completed but will need to be reconnected. "  -button1 "OK" -button2 "CANCEL" -defaultButton 1 -cancelButton 2 -timeout 60 -countdown` 

if [[ $banner == "2" ]]; then

    echo "User canceled the change."
    exit 1

fi


loggedinuser=`python -c 'from SystemConfiguration import SCDynamicStoreCopyConsoleUser; import sys; username = (SCDynamicStoreCopyConsoleUser(None, None, None) or [None])[0]; username = [username,""][username in [u"loginwindow", None, u""]]; sys.stdout.write(username + "\n");'`
logfile="/Library/Logs/dns_Oscar_config.log"


sudo -u "$loggedinuser" /usr/bin/osascript <<ENDofOSAscript


set apps to {"Pritunl"}
repeat with thisApp in apps
    tell application thisApp to quit
end repeat

ENDofOSAscript


if      [[ "$loggedinuser" = <local_admin> ]]
        then echo "`date`: Script should not be run as Oscadmin" >> "$logfile"
        exit

elif    [[ "$loggedinuser" = root ]]
        then echo "`date`: Script should not be run as Root" >> "$logfile"
        exit
   
else 
	    /usr/bin/sudo -u "$loggedinuser" networksetup -setdnsservers Wi-Fi 208.67.222.222 208.67.220.220 >> "$logfile"
        echo "`date`: Changed DNS Servers To OpenDNS" >> "$logfile"

fi

sudo -u "$loggedinuser" open -a Pritunl.app 

sudo dscacheutil -flushcache
sudo killall -HUP mDNSResponder

exit
0
 


