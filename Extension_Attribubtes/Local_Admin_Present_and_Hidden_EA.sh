#!/bin/sh

if [[ $(dscl . list /Users UniqueID | awk '$2 < 501 {print $1}' | grep -i "^account_name$") ]]; then
      echo "<result>Present</result>"
else
      echo "<result>Not Present</result>"
fi