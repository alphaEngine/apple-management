#!/bin/sh
currentUser=`last -1 -t console | awk '{print $1}'`

if [ -f "/usr/bin/mcxquery" ]; then
result=`/usr/bin/mcxquery -user "$currentUser" | grep -A 1 -w com.apple.screensaver | tail -1 | awk '{print $5}'`
if [ "$result" == "0" ]; then
	result="False"
elif [ "$result" == "1" ]; then
	result="True"
else
	result="Not set."
fi	
fi

echo "<result>$result</result>"
	