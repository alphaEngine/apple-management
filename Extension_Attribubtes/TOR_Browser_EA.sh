#!/bin/sh

TorInstances=$( mdfind 'kMDItemCopyright == "Tor Browser*" || kMDItemCFBundleIdentifier == "org.mozilla.tor browser"')

echo "<result>${TorInstances}</result>"